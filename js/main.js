"use strict";

/*

1.Опишіть своїми словами що таке Document Object Model (DOM)

    Це структура документу, яка представлена у вигляді ієрархіі (дерева) з вузлами-елементами (тегі) та текстовими вузлами. 

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerText всі додає текстом, innerHTML використовує синтаксис HTML та створює елементи.

    Приклад:

    let p = "<p>Hi</p>";

    document.body.innerText += p; (на сторінку буде додано текстова сторка "<p>Hi</p>")
    document.body.innerHTML += p; (буде створений тег <p> із текстом "Hi")

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    За допомогою вбудованого об'єкту document з додаванням шляху розташування (body.header/main/.....) 
    та необхідного методу або методів для отримання/опрацювання даних.

    Залежить від необхідного результату. До найкращіх взагалі, вважаю, можна віднести за наступними крітеріями:

    - об'єм написаного коду;
    - час виконання;
    
*/

const allParagraphs = document.querySelectorAll('p');

allParagraphs.forEach((e) =>{
    e.style.backgroundColor = '#ff0000';
    
});

const optionsList = document.getElementById('optionsList');

console.log(optionsList);
console.log(optionsList.parentNode);

const optionsListNamesAndTypes = optionsList.childNodes

optionsListNamesAndTypes.forEach((e) =>{
    console.log(`Name is: ${e.nodeName} \nType is: ${e.nodeType}`)
})

const testParagraph = document.getElementById('testParagraph').innerText = 'This is a paragraph';

const mainHeaderElement = document.querySelectorAll('.main-header li');

console.log(mainHeaderElement);

mainHeaderElement.forEach(e => {
    e.className = 'nav-item'
    
});

console.log(mainHeaderElement);

const sectionTitileClass = document.querySelectorAll('.section-title');
console.log(sectionTitileClass);

sectionTitileClass.forEach(e => {
    e.classList.toggle('section-title')
    
});

console.log(sectionTitileClass);